<?php

namespace Module\Test\Api;

interface StoreRepositoryInterface
{
    /**
     * Save store.
     *
     * @param \Module\Test\Api\Data\StoreInterface $store
     * @return \Module\Test\Api\Data\StoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Module\Test\Api\Data\StoreInterface $store);

    /**
     * Retrieve store.
     *
     * @param int $storeId
     * @return \Module\Test\Api\Data\StoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storeId);

    /**
     * Retrieve stores matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Module\Test\Api\Data\StoreSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete page.
     *
     * @param \Module\Test\Api\Data\StoreInterface $store
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Module\Test\Api\Data\StoreInterface $store);
}