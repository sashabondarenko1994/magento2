<?php

namespace Module\Test\Api\Data;

interface StoreInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID                = 'entity_id';
    const TITLE                    = 'title';
    const ADDRESS                  = 'address';
    const SCHEDULE                 = 'schedule';


    /**
     * Get ID
     *
     * @return int
     */
    public function getId();

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Get Address
     *
     * @return string
     */
    public function getAddress();

    /**
     * Get Schedule
     *
     * @return string
     */
    public function getSchedule();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Module\Test\Api\Data\StoreInterface
     */
    public function setId($id);

    /**
     * Get Title
     *
     * @param string $title
     * @return \Module\Test\Api\Data\StoreInterface
     */
    public function setTitle($title);

    /**
     * Get Address
     *
     * @param string $address
     * @return \Module\Test\Api\Data\StoreInterface
     */
    public function setAddress($address);

    /**
     * Get Schedule
     *
     * @param string $schedule
     * @return \Module\Test\Api\Data\StoreInterface
     */
    public function setSchedule($schedule);
}