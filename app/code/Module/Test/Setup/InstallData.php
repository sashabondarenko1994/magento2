<?php
/**
 * @author Elogic Team
 * @copyright Copyright (c) 2019 Elogic (https://elogic.co)
 */

namespace Module\Test\Setup;

use Magento\Cms\Model\Page;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Setup\EavSetup;
use Magento\Catalog\Model\Product;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;

/**
 * Class InstallData
 *
 * @package Diggecard\Giftcard\SetupquoteSetup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;


    /**
     * @var Page
     */
    private $cmsPage;
    /**
     * InstallData constructor.
     * @param EavSetupFactory $eavSetupFactory
     * @param Page $cmsPage
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        Page $cmsPage

    ) {
        $this->cmsPage = $cmsPage;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->cmsPage
            ->setIdentifier('store_module')
            ->setTitle('store_module')
            ->setContent('store_module welcome');

        $this->cmsPage->save();
    }
}
