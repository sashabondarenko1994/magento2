<?php
/**
 * @author Elogic Team
 * @copyright Copyright (c) 2019 Elogic (https://elogic.co)
 */

namespace Module\Test\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class UpgradeSchema
 *
 * @package Module\Test\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();

        $connection = $installer->getConnection();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $connection->addColumn(
                $installer->getTable('store_module'),
                'schedule',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 1023,
                    'nullable' => true,
                    'comment' => 'Working schedule'
                ]
            );
        }
        $installer->endSetup();
    }
}
