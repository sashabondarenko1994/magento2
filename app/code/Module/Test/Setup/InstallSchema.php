<?php
/**
 * @author Elogic Team
 * @copyright Copyright (c) 2019 Elogic (https://elogic.co)
 */

namespace Module\Test\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Zend_Db_Exception;

/**
 * Class InstallSchema
 *
 * @package Module\Test\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var EavSetup
     */
    private $eavSetup;

    /**
     * @param EavSetup $eavSetup
     * @param QuoteSetupFactory $setupFactory
     */
    public function __construct(
        EavSetup $eavSetup
    ) {
        $this->eavSetup = $eavSetup;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @throws Zend_Db_Exception
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'Giftcard'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('store_module')
        )->addColumn(
            'entity_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Store Id'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            36,
            ['nullable' => false],
            'Giftcard Qr CodeStore Title'
        )->addColumn(
            'address',
            Table::TYPE_TEXT,
            1024,
            ['nullable' => false],
            'Address'
        )->setComment('Module Store Table');
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
