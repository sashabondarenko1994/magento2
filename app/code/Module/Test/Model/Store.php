<?php

namespace Module\Test\Model;

use Module\Test\Api\Data\StoreInterface;
use Magento\Framework\Model\AbstractModel;


class Store extends AbstractModel implements StoreInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Module\Test\Model\ResourceModel\Store::class);
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Get Address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * Get Schedule
     *
     * @return string
     */
    public function getSchedule()
    {
        return $this->getData(self::SCHEDULE);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Module\Test\Api\Data\StoreInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get Title
     *
     * @param string $title
     * @return \Module\Test\Api\Data\StoreInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get Address
     *
     * @param string $address
     * @return \Module\Test\Api\Data\StoreInterface
     */
    public function setAddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * Get Schedule
     *
     * @param string $schedule
     * @return \Module\Test\Api\Data\StoreInterface
     */
    public function setSchedule($schedule)
    {
        return $this->setData(self::SCHEDULE, $schedule);
    }
}
