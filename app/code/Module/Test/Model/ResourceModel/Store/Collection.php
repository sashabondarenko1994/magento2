<?php

namespace Module\Test\Model\ResourceModel\Store;

use \Magento\Cms\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = StoreInterface::ENTITY_ID;


    /**
     * Define resource model
     *
     * @return void
     *
     */
    protected function _construct()
    {
        $this->_init(\Module\Test\Model\Store::class, \Module\Test\Model\ResourceModel\Store::class);
    }

    /**
     * Add filter by store
     *
     * @param int|array|\Magento\Store\Model\Store $store
     * @param bool $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        return $this;
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }
}
