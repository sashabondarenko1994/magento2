<?php

namespace Module\Test\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;


class Store extends AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('store_module', 'entity_id');
    }
}

