<?php

namespace Module\Test\Model;

use Module\Test\Api\Data;
use Module\Test\Api\StoreRepositoryInterface;
use Module\Test\Api\Data\StoreInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Module\Test\Model\ResourceModel\Store as ResourceStore;
use Module\Test\Model\StoreFactory as StoreFactory;
use Module\Test\Model\ResourceModel\Store\CollectionFactory as StoreCollectionFactory;

class StoreRepository implements StoreRepositoryInterface
{
    protected $resource;
    protected $storeFactory;
    protected $storeCollectionFactory;
    protected $collectionProcessor;
    protected $searchResultsFactory;

    public function __construct(
        ResourceStore $resource,
        StoreFactory $storeFactory,
        StoreCollectionFactory $storeCollectionFactory,
        Data\StoreSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor = null
    )
    {
        $this->resource = $resource;
        $this->storeFactory = $storeFactory;
        $this->storeCollectionFactory = $storeCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }


    /**
     * Save store.
     *
     * @param \Module\Test\Api\Data\StoreInterface $store
     * @return \Module\Test\Api\Data\StoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(StoreInterface $store)
    {
        try {
            $this->resource->save($store);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the page: %1', $exception->getMessage()),
                $exception
            );
        }
        return $store;
    }

    /**
     * Retrieve store.
     *
     * @param int $storeId
     * @return \Module\Test\Api\Data\StoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storeId)
    {
        $store = $this->storeFactory->create();
        $store->load($storeId);
        if (!$store->getId()) {
            throw new NoSuchEntityException(__('The CMS page with the "%1" ID doesn\'t exist.', $storeId));
        }
        return $store;
    }

    /**
     * Retrieve stores matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Module\Test\Api\Data\StoreSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Page\Collection $collection */
        $collection = $this->storeCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\StoreSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete store.
     *
     * @param \Module\Test\Api\Data\StoreInterface $store
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(StoreInterface $store)
    {
        try {
            $this->resource->delete($store);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the page: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }
}