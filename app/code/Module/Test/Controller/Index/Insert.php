<?php

namespace Module\Test\Controller\Index;

use Module\Test\Api\StoreRepositoryInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Insert extends Action
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * Index constructor.
     * @param \Elogic\Storelocator\Api\StoreRepositoryInterface $storelocator
     * @param PageFactory $pageFactory
     * @param Context $context
     */
    public function __construct(
        StoreRepositoryInterface $storelocator,
        PageFactory $pageFactory,
        Context $context
    ) {
        $this->store = $storelocator;
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {

        return  $resultPage = $this->pageFactory->create();
    }
}
