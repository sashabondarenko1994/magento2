<?php


namespace Module\Test\Controller\Index;

use Magento\Framework\App\Action\Action;


class Delete extends Action
{
    protected $_pageFactory;
    protected $_request;
    protected $_storeFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\App\Request\Http $request,
        \Module\Test\Model\StoreFactory $storeFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_request = $request;
        $this->_storeFactory = $storeFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->_request->getParam('id');
        $post = $this->_storeFactory->create();
        $result = $post->setId($id);
        $result = $result->delete();
        return $this->_redirect('testing/index/index');
    }
}