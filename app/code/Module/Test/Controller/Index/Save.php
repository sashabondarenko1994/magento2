<?php


namespace Module\Test\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends Action
{
    protected $_pageFactory;
    protected $_storeFactory;
    protected $_filesystem;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Module\Test\Model\StoreFactory $storeFactory,
        \Magento\Framework\Filesystem $filesystem
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_storeFactory = $storeFactory;
        $this->_filesystem = $filesystem;
        return parent::__construct($context);
    }

    public function execute()
    {
        if ($this->getRequest()->isPost()) {
            $input = $this->getRequest()->getPostValue();
            $store = $this->_storeFactory->create();

            if($input['entity_id']){
                $store->setId($input['entity_id']);
                $store->setTitle($input['title']);
                $store->setAddress($input['address']);
                $store->setSchedule($input['schedule']);
                $store->save();
            }else{
                $store->setData($input)->save();
            }

            return $this->_redirect('testing/index/index');
        }
    }
}