<?php

namespace Module\Test\Controller\Adminhtml\Store;

use Module\Test\Api\Data\StoreInterfaceFactory;
use Module\Test\Api\StoreRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Module\Test\Api\Data\StoreInterface;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Module_Test::all";

    /**
     * @var StoreInterfaceFactory
     */
    private $storeFactory;

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param StoreInterfaceFactory $storeFactory
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        Context $context,
        StoreInterfaceFactory $storeFactory,
        StoreRepositoryInterface $storeRepository
    ) {
        parent::__construct($context);
        $this->storeFactory = $storeFactory;
        $this->storeRepository = $storeRepository;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var StoreInterface $store */
        $store = $this->storeFactory->create();
        if (isset($data['entity_id']))
            $store->setId($data['entity_id']);
        $store
            ->setTitle($data['title'])
            ->setAddress($data['address'])
            ->setSchedule($data['schedule']);
        $this->storeRepository->save($store);
        $this->messageManager->addSuccess(__("The store was saved success."));
        $this->_redirect("storelocator/store/index");
    }
}
