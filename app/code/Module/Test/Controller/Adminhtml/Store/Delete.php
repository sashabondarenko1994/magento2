<?php

namespace Module\Test\Controller\Adminhtml\Store;

use Magento\Backend\App\Action\Context;
use Module\Test\Api\StoreRepositoryInterface;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Module_Test::all";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        Context $context,
        StoreRepositoryInterface $storeRepository
    ) {
        parent::__construct($context);
        $this->storeRepository = $storeRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        if ($id) {
            $store = $this->storeRepository->getById($id);
            try {
                $this->storeRepository->delete($store);
                $this->messageManager->addSuccess(__("The store was deleted success."));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__("Couldn't deleted the store."));
            }
        } else {
            $this->messageManager->addErrorMessage(__("Can't find the store."));
        }
        $this->_redirect("storelocator/store/index");
    }
}
